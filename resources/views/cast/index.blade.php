@extends('layout.template')
@section('judul')
Data Cast
@endsection

@section('content')

<a class="btn btn-success font-weight-bold" type="button" href="/cast/create">
    Create
</a>
<table class="table mt-3 text-center">
  <thead class="thead-dark">
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama</th>
      <th scope="col">Umur</th>
      <th scope="col">Bio</th>
      <th scope="col">Aksi</th>
    </tr>
  </thead>
  <tbody>
      @forelse ($show as $show=>$value)
    <tr>
      <th scope="row">{{$show + 1}}</th>
      <td>{{$value->nama}}</td>
      <td>{{$value->umur}}</td>
      <td>{{$value->bio}}</td>
      <td>
        <div class="row justify-content-center">
          <a href="/cast/{{$value->id}}" class="btn btn-info m-1">Show</a>
          <a href="/cast/{{$value->id}}/edit" class="btn btn-warning m-1">Update</a>
          <!-- <a href="/cast/{{$value->id}}" class="badge badge-danger">
            @method('delete')
            Delete
          </a> -->
          <form action="/cast/{{$value->id}}" method="post">
            @method('delete')
            @csrf
            <input type="submit" class="btn btn-danger m-1" value='Delete'>
          </form>
          </div>
        </td>
        @empty
    </tr>
    @endforelse
  </tbody>
</table>



@endsection