@extends('layout.template')
@section('judul')
Create Cast
@endsection

@section('content')

<form class="needs-validation" action="/cast" method="POST">
    @csrf
  <div class="form-row">
    <div class="col-md-6 mb-3">
      <label for="validationTooltip01">Nama</label>
      <input type="text" class="form-control" placeholder="Nama Kamu Siapa?" name="nama" required>
      @error('nama')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
    <div class="col-md-6 mb-3">
      <label for="validationTooltip02">Umur</label>
      <input type="text" class="form-control" name="umur"ss placeholder="Umur Kamu Berapa Tahun?" required>
      @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
  </div>
  <div class="form-row">
    <div class="col mb-3">
      <label for="validationTooltip03">Bio</label>
      <textarea class="form-control" name="bio" placeholder="Tulis Apapun Tentang Kamu" cols="30" rows="10" required></textarea>
      @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
  </div>
  <button class="btn btn-primary" type="submit">Simpan</button>
</form>

@endsection