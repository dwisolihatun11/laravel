<!doctype html>
<html lang="en">
<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/css/bootstrap.min.css" integrity="sha384-zCbKRCUGaJDkqS1kPbPd7TveP5iyJE0EjAuZQTgFLD2ylzuqKfdKlfG/eSrtxUkn" crossorigin="anonymous">
<title>Create Data</title>
</head>
<body>

<h2>Create Data Game</h2>

<!-- //Code disini -->
<form class="needs-validation" action="/game" method="POST">
    @csrf
  <div class="form-row">
    <div class="col-md-6 mb-3">
      <label for="validationTooltip01">Nama</label>
      <input type="text" class="form-control" name="name" required>
      @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
    <div class="col-md-6 mb-3">
      <label for="validationTooltip02">Gameplay</label>
      <textarea class="form-control" name="game_play" cols="30" rows="1" required></textarea>
      @error('game_play')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
    <div class="col-md-6 mb-3">
        <label for="validationTooltip01">Developer</label>
        <input type="text" class="form-control" name="developer" required>
        @error('developer')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <div class="col-md-6 mb-3">
        <label for="validationTooltip01">Year</label>
        <input type="number" class="form-control" name="year" required>
        @error('year')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
  </div>
  <button class="btn btn-primary" type="submit">Simpan</button>
</form>



<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-fQybjgWLrvvRgtW6bFlB7jaZrFsaBXjsOMm/tB9LTS58ONXgqbR9W8oWht/amnpF" crossorigin="anonymous"></script>
</body>
</html>

