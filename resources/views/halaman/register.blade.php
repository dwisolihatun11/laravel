<!DOCTYPE html>
<html>
<head>
    <title>Buat Akun Baru</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcome" method="post">
        @csrf
        <label for="first_name">First Name:</label><br>
        <input type="text" id="first_name" name="first_name"><br>
        <label for="last_name">Last Name:</label><br>
        <input type="text" id="last_name" name="last_name"><br><br>
        <label for="gender">Gender:</label><br>
        <input type="radio" id="male" name="gender" value="Male">
        <label for="male">Male</label><br>
        <input type="radio" id="female" name="gender" value="Female">
        <label for="female">Female</label><br>
        <input type="radio" id="other" name="gender" value="Other">
        <label for="other">Other</label><br><br>
        <label for="nationality">Nationality:</label><br>
        <select name="nationality" id="nationality">
            <option value="indonesia">Indonesia</option>
            <option value="amerika">Amerika</option>
            <option value="inggris">Inggris</option>
        </select> <br> <br>
        <label for="speak">Language Spoken:</label><br>
        <input type="checkbox" id="indo" name="bahasa" value="Bahasa Indonesia">
        <label for="indo">Bahasa Indonesia</label><br>
        <input type="checkbox" id="english" name="bahasa" value="English">
        <label for="english">English</label><br>
        <input type="checkbox" id="other" name="bahsa" value="Other">
        <label for="other">Other</label><br><br>
        <label for="bio">Bio:</label><br>
        <textarea name="bio" id="bio" cols="30" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>