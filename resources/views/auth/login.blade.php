@extends('layouts.auth')
@section('judul')
Login
@endsection

@section('content')
    <div class="card-body">     

      <form action="{{route('login')}}" method="post">
          @csrf
        <div class="input-group mb-3">
          <input type="email" name="email" class="form-control" placeholder="Email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
            @error('email')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" name="password" class="form-control" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
            @error('password')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
          </div>
        </div>
            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
        </div>
      </form>
    </div>
    <!-- /.card-body -->

@endsection
