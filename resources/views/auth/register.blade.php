@extends('layouts.auth')
@section('judul')
Registrasi
@endsection

@section('content')
    <div class="card-body">
      <p class="login-box-msg">Registrasi Akun Baru</p>

      <form action="{{route('register')}}" method="post">
          @csrf
        <div class="input-group mb-3">
          <input type="text" name="name" class="form-control" placeholder="Nama Lengkap">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
            @error('name')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="email" name="email" class="form-control" placeholder="Email">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
            @error('email')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="number" name="umur" class="form-control" placeholder="Umur">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
            @error('umur')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="text" name="alamat" class="form-control" placeholder="Alamat">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
            @error('alamat')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>
        </div>
        <div class="input-group mb-3">
          <textarea name="bio" class="form-control" placeholder="Bio"></textarea>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
            @error('bio')
                <div class="alert alert-danger">{{ $message }}</div>
            @enderror
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" name="password" class="form-control" placeholder="Kata Sandi">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
            @error('password')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" name="password_confirmation" class="form-control" placeholder="Ulangi Kata Sandi">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
            @error('password_confirmation')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
          </div>
        </div>
        
          <button type="submit" class="btn btn-primary btn-block">Register</button>
        

          <!-- /.col -->
</form>
    </div>
    <!-- /.form-box -->
  
@endsection
