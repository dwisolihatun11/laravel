@extends('layout.template')
@section('judul')
Ubah Profile
@endsection

@section('content')

<form class="needs-validation" action="/profile/{{$profile->id}}" method="POST">
    @csrf
    @method('put')
  
      
    <div class="col mb-3">
      <label for="validationTooltip02">Nama Pengguna</label>
      <input type="text" class="form-control" value="{{$profile->user->name}}" disabled>
      
    </div>
    <div class="col mb-3">
      <label for="validationTooltip02">Email Pengguna</label>
      <input type="text" class="form-control" value="{{$profile->user->email}}" disabled>
    </div>
    <div class="col mb-3">
      <label for="validationTooltip02">Umur</label>
      <input type="number" class="form-control" name="umur" value="{{$profile->umur}}" required>
      @error('umur')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
  
  <div class="form-row">
    <div class="col mb-3">
      <label for="validationTooltip03">Bio</label>
      <textarea class="form-control" name="bio" cols="30" rows="10" required>{{$profile->bio}}</textarea>
      @error('bio')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
  </div>
  <div class="form-row">
    <div class="col mb-3">
      <label for="validationTooltip03">Alamat</label>
      <textarea class="form-control" name="alamat" cols="30" rows="10" required>{{$profile->alamat}}</textarea>
      @error('alamat')
        <div class="alert alert-danger">{{ $message }}</div>
      @enderror
    </div>
  </div>
  <button class="btn btn-primary" type="submit">Ubah</button>
</form>
@endsection