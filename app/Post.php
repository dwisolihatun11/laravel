<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = "post"; // Mengarahkan model ke tebel post pada database
    protected $fillable = ["title", "body"]; // berfungsi memberitahu kolom apa saja yang akan di manipulasi
}
