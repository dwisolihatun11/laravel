<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cast extends Model
{
    protected $table = "cast"; // Mengarahkan model ke tebel cast pada database
    protected $fillable = ["nama", "umur", "bio"]; // berfungsi memberitahu kolom apa saja yang akan di manipulasi
}
