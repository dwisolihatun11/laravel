<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Film extends Model
{
    protected $table = "film"; // Mengarahkan model ke tebel profile pada database
    protected $fillable = ["judul", "ringkasan", "tahun", "poster", "genre_id"]; // berfungsi memberitahu kolom apa saja yang akan di manipulasi

    public function genre()
    {
        return $this->belongsTo('App\User');
    }
}
