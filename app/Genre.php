<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
    protected $table = "genre"; // Mengarahkan model ke tebel profile pada database
    protected $fillable = ["nama"]; // berfungsi memberitahu kolom apa saja yang akan di manipulasi

    public function film()
    {
        return $this->hasMany('App\Film');
    }
}
