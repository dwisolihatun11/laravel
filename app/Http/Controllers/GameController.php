<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use DB;

class GameController extends Controller
{
    public function index() // Read data Game
    {
        $dataGame = DB::table('game')->get();
        return view('game.index', compact('dataGame'));
    }

    public function create() // Create data Game
    {
        return view('game.create');
    }

    public function store(Request $newGame) // Insert data Game baru ke databaase
    {
        $newGame->validate([
            'name' => 'required',
            'game_play' => 'required',
            'developer' => 'required',
            'year' => 'required'
        ]);

        $query = DB::table('game')->insert([
            "name" => $newGame["name"],
            "game_play" => $newGame["game_play"],
            "developer" => $newGame["developer"],
            "year" => $newGame["year"]
        ]);

        return redirect('/game');
    }

    public function show($id) // Menampilkan data game berdasarkan id
    {
        $show = DB::table('game')->where('id', $id)->first();
        return view('game.show', compact('show'));
    }
    
    public function edit($id) // Menampilkan data yang akan di edit berdasarkan id
    {
        $show = DB::table('game')->where('id', $id)->first();
        return view('game.edit', compact('show'));
    }

    public function update($id, Request $editGame) // Mengubah data berdasarkan id
    {
        $editGame->validate([
            'name' => 'required',
            'game_play' => 'required',
            'developer' => 'required',
            'year' => 'required'
        ]);
        $query = DB::table('game')->where('id', $id)->update([
            'name' => $editGame['name'],
            'game_play' => $editGame['game_play'],
            'developer' => $editGame['developer'],
            'year' => $editGame['year']
        ]);
        return redirect('/game');
    }

    public function destroy($id) //Menghapus data game berdasarkan id
    {
        $show = DB::table('game')->where('id', $id)->delete();
        return redirect('game');
    }
}
