<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register(){
        return view('halaman.register');
    }

    public function welcome(Request $input){
        // dd($input->all());
        $first_name = $input['first_name'];
        $last_name = $input['last_name'];
        return view('halaman.welcome', compact('first_name', 'last_name'));
    }
}
