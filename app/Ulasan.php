<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ulasan extends Model
{
    protected $table = "kritik";
    protected $fillable = ['user_id', 'film_id', 'isi', 'point'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
