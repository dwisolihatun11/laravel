<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = "profile"; // Mengarahkan model ke tebel profile pada database
    protected $fillable = ["umur", "bio", "alamat", "user_id"]; // berfungsi memberitahu kolom apa saja yang akan di manipulasi

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
