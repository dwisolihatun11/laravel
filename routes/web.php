<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@home');
Route::get('/register', 'AuthController@register');
Route::post('/welcome', 'AuthController@welcome');

// Templating

Route::get('/table', function()
{
    return view('templating.table');
});

Route::get('/data-tables', function()
{
    return view('templating.data-table');
});

// CRUD Game Quiz 
Route::get('/game', 'GameController@index');
Route::get('game/create', 'GameController@create');
Route::post('/game', 'GameController@store');
Route::get('/game/{game_id}', 'GameController@show');
Route::get('/game/{game_id}/edit', 'GameController@edit');
Route::put('/game/{game_id}', 'GameController@update');
Route::delete('/game/{game_id}', 'GameController@destroy');

// Laravel CRUD (dengan Eloquent ORM)
Route::resource('post', 'PostController');
Route::get('/post', 'PostController@index');
Route::get('post/create', 'PostController@create');
Route::post('/post', 'PostController@store');
Route::get('/post/{post_id}', 'PostController@show');
Route::get('/post/{post_id}/edit', 'PostController@edit');
Route::put('/post/{post_id}', 'PostController@update');
Route::delete('/post/{post_id}', 'PostController@destroy');

// Authentication and Middleware

Route::resource('cast', 'CastController');
Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    // CRUD Cast
    Route::get('/cast/create', 'CastController@create');
    Route::post('/cast', 'CastController@store');
    Route::get('/cast/{cast_id}/edit', 'CastController@edit');
    Route::put('/cast/{cast_id}', 'CastController@update');
    Route::delete('/cast/{cast_id}', 'CastController@destroy');
    Route::get('/cast', 'CastController@index');
    Route::get('/cast/{cast_id}', 'CastController@show');

    // Update Profile
    Route::resource('profile', 'ProfileController');    
});

// Route::get('/home', 'HomeController@index')->name('home');
